<?php
include 'functions.php';
include 'header.php';
$row_thread = details_thread($_GET['id']);
$row_reply = tampil_reply($_GET['id']);
?>


<div style="margin: 0px 21% 0px 21%; padding: 90px 15px 15px 15px; background-color: lavender;">
 <div class="card">
  <div class="card-header border border-info">
   <img src="img/profimg.png" id="profimg"/>  <?= $row_thread['nama']; ?>
   <div class="float-right pb-0"><?= $row_thread['tgl']; ?></div>
  </div>
 <div class="card-body border border-info">
    <h5 class="card-title"><?= $row_thread['judul']; ?></h5><hr>
        <p class="card-text text-justify"><?= $row_thread['isi']; ?></p>
         
      </div>
    </div>

        <!-- Kolom Jawaban -->
<?php foreach ($row_reply as $row): ?>
    <div class="media p-2">
      <img class="d-flex rounded-circle avatar z-depth-1-half mr-2 mb-0" src="img/profimg.png" alt="Avatar">
          <div class="media-body">
            <h6 class="mt-0 mb-0 font-weight-bold blue-text"><?= $row['nama']; ?></h6>
            <?= $row['reply']; ?>
          </div>
        </div>
<?php endforeach ?>


        <!-- Kolom Komentar (texarea) -->
        <form action="" method="post">
        <div class="form-group shadow-textarea mt-2 mb-2">
          <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Nama anda.." name="nama">
            <textarea class="form-control z-depth-1" id="exampleFormControlTextarea6" name="reply" rows="3" placeholder="Tulis komentar anda disini....."></textarea></div>

            <!-- Button -->
            <button type="submit" name="btnreply" class="btn btn-primary btn-sm">Komentari</button><hr>
        </form>
        <?php
        if (isset($_POST['btnreply'])) {
        	post_reply($_POST, $_GET['id']);
        	echo "<meta http-equiv='refresh' content='0.1;url=details.php?id=".$row_thread['id']."'>";
        }
        ?>
            <a href="index.php"><button class="btn btn-default">Kembali</button></a>  
    </div>
</div>



</div>

<?php include 'footer.php';?>