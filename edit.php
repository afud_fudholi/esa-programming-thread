<?php 
	include 'functions.php';
	include 'header.php';
  $row = details_thread($_GET['id'])
 ?>

  <div class="form-group" style="margin: 100px 25% 0px 25%;">
    <label for="exampleFormControlInput1">Edit Thread "<?= $row['judul']?>"</label>
    <form method="post">
      <input type="text" class="form-control" id="exampleFormControlInput1" name="judul" value="<?= $row['judul'] ?>">
        <textarea class="form-control" id="editor" rows="3" name="isi">
        <?= $row['isi'] ?>
        </textarea>
      <button type="submit" name="btnedit" class="btn btn-info p-1 mt-2">Edit</button> 
  </div>
 </form>
 <?php if (isset($_POST['btnedit'])) {
  edit_thread($_POST, $_GET['id']);
  echo "<meta http-equiv='refresh' content='0.1;url=index.php'>";
 }

 ?>




 <!-- Footer -->
<footer class="page-footer font-small bg-secondary fixed-bottom">
  <!-- Copyright -->
  <div class="footer-copyright text-center py-3">© 2019 Copyright | created by Afudfudholi
  </div>
</footer>

<!-- CkEditor -->
<script src="assets/js/ckeditor.js"></script>
    <script>
    ClassicEditor
    .create( document.querySelector( '#editor' ), {
      // toolbar: [ 'heading', '|', 'bold', 'italic', 'link' ]
    } )
    .then( editor => {
      window.editor = editor;
    } )
    .catch( err => {
      console.error( err.stack );
    } );
  </script>