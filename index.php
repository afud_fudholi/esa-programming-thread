<?php 
  include 'functions.php';
  include 'header.php';

$no=1;
// Ambil data dari tabel thread
$result = mysqli_query($conn, "SELECT * FROM thread");
 ?>

<!-- Input Thread -->
<?php 
  if (isset($_POST['nama'])) {
    input_thread($_POST['nama'],$_POST['judul'], $_POST['isi']);
    echo "<meta http-equiv='refresh' content='0.1;url=index.php'>";
  }
 ?>

<div style="margin: 0px 21% 0px 21%; padding: 90px 15px 15px 15px; background-color: lavender;">

<!-- Tambahkan Thread -->
<form action="" method="POST">
  <div class="form-group">
    <label for="exampleFormControlInput1">Tambahkan Thread</label>
     <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="nama...." name="nama">
      <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="judul...." name="judul">
        <textarea class="form-control" id="editor" rows="3" placeholder="Tulis thread...." name="isi"></textarea>
      <button type="submit" class="btn btn-info p-1 mt-2">Tambah</button> 
  </div>
</form>
<hr>
<?php while( $row = mysqli_fetch_assoc($result)) : ?>
<h5 class="mt-5">Thread <?php echo $no++;?></h5>
<!-- Thread 1 -->
<div class="card">
  <div class="card-header border border-info">
   <img src="img/profimg.png" id="profimg"/>  <?= $row["nama"]; ?>
</div>
  <div class="card-body border border-info">
     <h5 class="card-title"><?= $row["judul"]; ?></h5><hr>
        <p class="card-text text-justify"><?= $row["isi"]; ?></p><br><hr>
         <?= $row["tgl"]; ?>
          <div class="float-right">
            <a href="details.php?id=<?= $row['id']; ?>">Details</a> |
            <a href="edit.php?id=<?= $row['id']; ?>">Edit</a> |
            <a href="hapus.php?id=<?= $row['id']; ?>" onclick="return confirm('Anda yakin ingin menghapus ?')">Hapus</a>
          </div>
      </div>
    </div>
<?php endwhile; ?>
  </div>
</div>

 <?php 
  include 'footer.php';
 ?>